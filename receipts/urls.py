from django.urls import path
from receipts.views import (
    ReceiptView,
    CreateReceipt,
    ExpenseView,
    AccountView,
    CreateExpense,
    CreateAccount,
)

urlpatterns = [
    path("", ReceiptView, name="home"),
    path("create/", CreateReceipt, name="create_receipt"),
    path("categories/", ExpenseView, name="category_list"),
    path("accounts/", AccountView, name="account_list"),
    path("categories/create/", CreateExpense, name="create_category"),
    path("accounts/create/", CreateAccount, name="create_account"),
]
