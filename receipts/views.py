from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


# Create your views here.
@login_required()
def ReceiptView(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receipt.html", context)


@login_required()
def CreateReceipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.purchaser = request.user
            receipts.save()

        return redirect(ReceiptView)

    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required()
def ExpenseView(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expenses": expenses}
    return render(request, "receipts/category.html", context)


@login_required()
def AccountView(request):
    account = Account.objects.filter(owner=request.user)
    context = {"account": account}
    return render(request, "receipts/account.html", context)


@login_required()
def CreateExpense(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)

        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()

        return redirect(ExpenseView)

    else:
        form = ExpenseForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createexpense.html", context)


@login_required()
def CreateAccount(request):
    if request.method == "POST":
        form = AccountForm(request.POST)

        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()

        return redirect(AccountView)

    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createaccount.html", context)
